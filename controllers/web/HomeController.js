const { User } = require('../../models')
const bcrypt = require("bcrypt")

class HomeController{

  index = (req, res) => {
    User.findAll()
    .then(users => {
      res.render("../views/index", {
        content: './pages/userList',
        users: users
      })
    })
  }

  add = (req, res) => {
    res.render("../views/index", {
      content: './pages/addList'
    })
  }

  saveUser = async (req, res) => {
    const salt = await bcrypt.genSalt(10)

    User.create({
      name: req.body.name,
      username : req.body.username,
      age: req.body.age,
      password: await bcrypt.hash(req.body.password, salt)
    })
    .then(user => {
      res.redirect('/')
    }).catch(err => {
      res.status(422).json("Can't Created User")
    })
  }

  editUser = (req, res) => {
    User.findOne({
      where: {
        id: req.params.id
      }
    })
    .then(user => {
      res.render("../views/index", {
        content : './pages/editList',
        user: user
      })
    })
  }

  updateUser = (req, res) => {
    User.update({
      name: req.body.name,
      username : req.body.username,
      age: req.body.age,
      password: req.body.password
    }, {
      where: { id: req.params.id }
    })
    .then(user => {
      res.redirect('/')
    }).catch(err => {
      res.status(422).json("Can't update user")
    })
  }

  delete = (req, res) => {
    User.destroy({
      where: { id: req.params.id }
    })
    .then(() => {
      res.redirect('/')
    })
  }
}

module.exports = HomeController