'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Users', [{
      name: 'John Doe',
      username : 'john',
      age: '20',
      password: 'mutho',
      createdAt: new Date,
      updatedAt: new Date
    }], {})
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Users', null, {});
  }
};

// sequelize db:seed:undo --seed name-of-seed-as-in-data ( ini diisi dengan sesuai nama file yang diseeder )
// sequelize db:migrate:undo:all --to XXXXXXXXXXXXXX-create-posts.js  ( rollback dari history terakhir sampai history ke berapa spesifiknya )